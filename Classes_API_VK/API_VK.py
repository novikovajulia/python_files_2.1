# Задача №1
# Пользователя нужно описать с помощью класса и реализовать метод поиска общих друзей, используя API VK.

# Задача №3
# Вывод print(user) должен выводить ссылку на профиль пользователя в сети VK

from urllib.parse import urlencode
import requests
import pprint


APP_ID = 0
token = ''


# AUTH_URL = 'https://oauth.vk.com/authorize'
# auth_data = {
#   'client_id': APP_ID,
#   'redirect_uri': 'https://oauth.vk.com/blank.html',
#   'display': 'page',
#   'scope': 'friends,status',
#   'response_type': 'token',
#   'v': 5.80
# }

# print('?'.join((AUTH_URL, urlencode(auth_data))))

class User:
    def __init__(self, user_id):
        self.user_id = user_id

    def request(command):
        response = requests.get('https://api.vk.com/method/' + command,
        params=dict(
          access_token=token,
          v=5.80,
          user_id=self.user_id,
          ))
        return response.json()

    def get_friends(self):
        return self.request('friends.get')['response']['items']

    def __and__(self, another_user):
        return set(self.get_friends()) & set(another_user.get_friends())

    def __str__(self):
        screen_name = self.request('account.getProfileInfo')['response']['screen_name']
        return 'https://vk.com/' + screen_name


user1 = User(user_id=1848657)
user2 = User(user_id=2448592)
common_friends = user1 & user2
print(common_friends)

print(user1)