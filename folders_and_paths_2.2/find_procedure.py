# Задание
# мне нужно отыскать файл среди десятков других
# я знаю некоторые части этого файла (на память или из другого источника)
# я ищу только среди .sql файлов
# 1. программа ожидает строку, которую будет искать (input())
# после того, как строка введена, программа ищет её во всех файлах
# выводит список найденных файлов построчно
# выводит количество найденных файлов
# 2. снова ожидает ввод
# поиск происходит только среди найденных на этапе 1
# 3. снова ожидает ввод
# ...
# Выход из программы программировать не нужно.
# Достаточно принудительно остановить, для этого можете нажать Ctrl + C

# Пример на настоящих данных

# python3 find_procedure.py
# Введите строку: INSERT
# ... большой список файлов ...
# Всего: 301
# Введите строку: APPLICATION_SETUP
# ... большой список файлов ...
# Всего: 26
# Введите строку: A400M
# ... большой список файлов ...
# Всего: 17
# Введите строку: 0.0
# Migrations/000_PSE_Application_setup.sql
# Migrations/100_1-32_PSE_Application_setup.sql
# Всего: 2
# Введите строку: 2.0
# Migrations/000_PSE_Application_setup.sql
# Всего: 1

# не забываем организовывать собственный код в функции

import os


migrations = 'Migrations'


def find_sql(all_files):
    sql_files = []
    for file in all_files:
        if file.endswith('.sql'):
            sql_files.append(file)
    return(sql_files)


def find_in_files(files_to_search, search_string):
    find_migrations = []
    for file in files_to_search:
        file_path = os.path.join(migrations_abs_path, file)
        with open(file_path) as o:
            if search_string in o.read():
                print(os.path.join(migrations, file))
                find_migrations.append(file)
    return find_migrations


current_dir = os.path.dirname(os.path.abspath(__file__))
migrations_abs_path = os.path.join(current_dir, migrations)
all_files = os.listdir(migrations_abs_path)
files_to_search = find_sql(all_files)

while True:
    search_string = input('Введите строку: ')
    find_migrations = find_in_files(files_to_search, search_string)
    files_to_search = find_migrations
    print('Всего: {}'.format(len(find_migrations)))
