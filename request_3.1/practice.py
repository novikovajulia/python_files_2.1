import requests
import os

def translate_it(text, lang):

    """
    YANDEX translation plugin

    docs: https://tech.yandex.ru/translate/doc/dg/reference/translate-docpage/

    https://translate.yandex.net/api/v1.5/tr.json/translate ?
    key=<API-ключ>
     & text=<переводимый текст>
     & lang=<направление перевода>
     & [format=<формат текста>]
     & [options=<опции перевода>]
     & [callback=<имя callback-функции>]

    :param text: <str> text for translation.
    :return: <str> translated text.
    """
    url = 'https://translate.yandex.net/api/v1.5/tr.json/translate'
    key = 'trnsl.1.1.20180802T194715Z.4122eb5bcd3e5e44.aeb5f0a71e960968195f7a1577739f8b52bb5062'

    params = {
        'key': key,
        'lang': lang,
        'text': text,
    }
    answer = requests.get(url, params=params)
    response = answer.json()
    # return ' '.join(response.get('text', []))
    return ' '.join(response['text'])


def detect_language(text):

    """
    Detect the language

    https://tech.yandex.com/translate/doc/dg/reference/detect-docpage/

    https://translate.yandex.net/api/v1.5/tr.json/detect ?
    key=<API key>
    & text=<text>
    & [hint=<list of probable text languages>]
    & [callback=<name of the callback function>]

    """
    url = 'https://translate.yandex.net/api/v1.5/tr.json/detect'
    key = 'trnsl.1.1.20180802T194715Z.4122eb5bcd3e5e44.aeb5f0a71e960968195f7a1577739f8b52bb5062'

    params = {
        'key': key,
        'text': text,
    }
    answer = requests.get(url, params=params)
    response = answer.json()
    return response['lang']


def find_txt(all_files):
    txt_files = []
    for file in all_files:
        if file.endswith('.txt'):
            txt_files.append(file)
    return(txt_files)


def open_file(input_path):
    with open(input_path, encoding='utf-8') as fr:
        text = fr.read()
    return text


def write_file(output_path, translated_text):
    with open(output_path, 'w', encoding='utf-8') as ru:
        ru.write(translated_text)


def main():
    folder_path = 'originals'
    originals = os.listdir(folder_path)
    txt_files = find_txt(originals)
    output_path = 'translations'
    for file in txt_files:
        print('Перевожу файл {}'.format(file))
        text = open_file(os.path.join(folder_path, file))
        lang = detect_language(text) + '-ru'
        translated_text = translate_it(text, lang)
        write_file(os.path.join(output_path, file), translated_text)


main()
