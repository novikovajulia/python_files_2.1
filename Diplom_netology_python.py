import requests
import pprint
import time
import json


MAX_NUM_USERS = 500
user_input = 'eshmargunov'


def request(command, additional_arguments):
    additional_arguments['access_token'] = token
    additional_arguments['v'] = 5.80
    num_repeats = 5
    for i in range(num_repeats):
        try:
            print('.', end='')
            response = requests.post('https://api.vk.com/method/' + command,
                                     data=additional_arguments)
            response_dict = response.json()
            response_dict['response']
            break
        except (requests.RequestException, KeyError) as e:
            if i == num_repeats - 1:
                if isinstance(e, KeyError):
                    print(response_dict)
                raise e
            else:
                time.sleep(1)
    return response_dict


class User:

    def __init__(self, user_id_or_screen_name):
        try:
            self.user_id = int(user_id_or_screen_name)
        except ValueError:
            self.user_id = self.screen_name_to_user_id(user_id_or_screen_name)

    def screen_name_to_user_id(self, screen_name):
        arguments = {'screen_name': screen_name}
        response = request('utils.resolveScreenName', arguments)
        return response['response']['object_id']

    def get_friends(self):
        arguments = {'user_id': self.user_id}
        return request('friends.get', arguments)['response']['items']

    def get_groups(self):
        arguments = {'user_id': self.user_id, 'extended': 1,
                     'fields': 'members_count'}
        return request('groups.get', arguments)['response']['items']


def any_user_in_group(group_id, user_ids):
    user_ids_strings = [str(id) for id in user_ids]

    while len(user_ids_strings) > 0:
        uids_str = ','.join(user_ids_strings[:MAX_NUM_USERS])
        user_ids_strings = user_ids_strings[MAX_NUM_USERS:]
        arguments = {'group_id': group_id, 'user_ids': uids_str}
        response = request('groups.isMember', arguments)['response']

        for unique_response in response:
            if unique_response['member'] == 1:
                return True
    return False


def find_spy_groups(user):
    user_friends = user.get_friends()
    user_groups = user.get_groups()
    spy_groups = []
    for group in user_groups:
        if not any_user_in_group(group['id'], user_friends):
            found_group = {'name': group['name'], 'gid': group['id'],
                           'members_count': group['members_count']}
            spy_groups.append(found_group)
    return spy_groups


if __name__ == '__main__':
    with open('password.json') as f:
        token = json.loads(f.read())['token']
    user = User(user_input)
    spy_groups = find_spy_groups(user)
    print('\n')
    with open('groups.json', 'w') as spy:
        json.dump(spy_groups, spy)
